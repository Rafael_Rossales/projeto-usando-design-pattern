/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern;

/**
 *
 * @author Rafael_Rossales
 */
public class CasaDeVidro {
    

    public void CasaDeVidro() {
        if (construirFundacao()) {
            System.out.println("Construindo fundacao");
            if (construirPilares()) {
                System.out.println("Construindo pilares de Vidro");
                if (construirParedes()) {
                    System.out.println("Construindo paredes de Vidro");
                    if (construirJanelas()) {
                        System.out.println("Construindo janelas de Vidro ");
                        if (construirTelhado()) {
                            System.out.println("Construindo Telhado");
                        } else {
                            System.out.println("Telhado não construido");
                        }
                    } else {
                        System.out.println("Janelas não construidas");

                    }
                } else {
                    System.out.println("Paredes não construidas");
                }

            } else {
                System.out.println("Pilares não construidos");
            }
        } else {
            System.out.println("Fundacao não construida");
        }

        System.out.println("Casa de Vidro construida!");

    }

    private boolean construirFundacao() {
        return true;
    }

    private boolean construirPilares() {
        return true;
    }

    private boolean construirParedes() {
        return true;
    }

    private boolean construirJanelas() {
        return true;
    }

    private boolean construirTelhado() {
        return true;
    }

}

